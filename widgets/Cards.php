<?php

namespace JMSystems\BackendExtensions\Widgets;


use Backend\Classes\WidgetBase;
use Backend\Facades\Backend;
use System\Classes\MediaLibrary;
use October\Rain\Router\Helper as RouterHelper;

class Cards extends WidgetBase {

    public $model;
    public $fields;
    public $recordUrl;
    public $recordsPerPage;
    public $noRecordsMessage;

    protected $showPagination;
    protected $currentPageNumber;
    protected $searchTerm;
    protected $records;

    public function init()
    {
        $this->fillFromConfig([
            'model',
            'fields',
            'recordUrl',
            'recordsPerPage',
            'noRecordsMessage',
        ]);

        $this->showPagination = $this->recordsPerPage && $this->recordsPerPage > 0;
    }

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('cards');
    }

    public function onRefresh()
    {
        $this->prepareVars();
        return [
            'bexCards_'.$this->getId() => $this->vars,
            '#'.$this->getId('Pagination') => $this->makePartial('cards_pagination')
        ];
    }

    public function onPaginate()
    {
        $this->currentPageNumber = post('page');
        return $this->onRefresh();
    }

    public function setSearchTerm($term) {
        $this->searchTerm = $term;
    }

    protected function loadAssets()
    {
        // required for all widgets, will be added only once
        // if any of bex widgets are added to the page
        $this->addJs('../../../assets/js/vendor.js', 'bex');
        $this->addJs('../../../assets/js/common.js', 'bex');

        // per widget assets
        $this->addJs('../../../assets/js/cards.widget.js', 'bex');
        $this->addCss('../../../assets/css/cards.css', 'bex');
    }

    protected function getRecords() {
        $query = $this->prepareQuery();

        if ($this->showPagination) {
            $currentPageNumber = $this->getCurrentPageNumber($query);
            $records = $query->paginate($this->recordsPerPage, $currentPageNumber);
        }
        else {
            $records = $query->get();
        }

        return $records;
    }

    protected function getCurrentPageNumber($query)
    {
        $currentPageNumber = $this->currentPageNumber;

        if (!$currentPageNumber && empty($this->searchTerm)) {
            $currentPageNumber = $this->getSession('lastVisitedPage');
        }

        $currentPageNumber = intval($currentPageNumber);

        if ($currentPageNumber > 1) {
            $count = $query->count();

            if ($count <= (($currentPageNumber - 1) * $this->recordsPerPage)) {
                $currentPageNumber = ceil($count / $this->recordsPerPage);
            }
        }

        return $currentPageNumber;
    }

    protected function prepareVars() {
        $records = $this->getRecords();

        if ($this->showPagination) {
            $this->vars['pageCurrent'] = $records->currentPage();

            $this->putSession('lastVisitedPage', $this->vars['pageCurrent']);
            $this->vars['recordTotal'] = $records->total();
            $this->vars['pageLast'] = $records->lastPage();
            $this->vars['pageFrom'] = $records->firstItem();
            $this->vars['pageTo'] = $records->lastItem();
        }
        else {
            $this->vars['recordTotal'] = $records->count();
            $this->vars['pageCurrent'] = 1;
        }

        $this->records = $records->map(function ($item) {
            $item->image = $item->image ? MediaLibrary::url($item->image) : null;
            $item->url = Backend::url(RouterHelper::replaceParameters($item, $this->recordUrl));
            return $item;
        });

        $this->vars['noRecordsMessage'] = $this->noRecordsMessage;
        $this->vars['showPagination'] = $this->showPagination;
        $this->vars['items'] = json_encode($this->records);
    }

    protected function prepareQuery() {
        $query = $this->model->newQuery();
        $primaryTable = $this->model->getTable();
        $selects = [
            $primaryTable.'.id',
        ];
        $searchableFields = [];

        foreach ($this->fields as $key => $field) {
            $selects[] = $primaryTable.'.'.$field.' as '.$key;

            if ($key !== 'image')
                $searchableFields[] = $primaryTable.'.'.$field;
        }

        if (isset($this->searchTerm) && !empty($this->searchTerm)) {
            $query->where(function ($innerQuery) use ($searchableFields) {
                foreach ($searchableFields as $field) {
                    $innerQuery->orWhere(function ($q) use ($field) {
                        $q->orSearchWhere($this->searchTerm, $field);
                    });
                }
            });
        }

        $query->addSelect($selects);
        return $query;
    }

}