<?php

namespace JMSystems\BackendExtensions\Behaviors;

use Backend\Classes\ControllerBehavior;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;

class CardsController extends ControllerBehavior
{

    public $cardsConfig;

    protected $config;
    protected $cards;
    protected $toolbar;

    public function __construct($controller)
    {
        parent::__construct($controller);
        $this->cardsConfig = $this->controller->cardsConfig;
    }

    public function index() {
        $this->controller->bodyClass = 'slim-container';
        $this->controller->pageTitle = $this->controller->pageTitle ?: Lang::get($this->cardsGetConfig()->title);

        $this->cards = $this->controller->vars['cards'] = $this->makeCards();
        $this->toolbar = $this->controller->vars['toolbar'] = $this->makeToolbar();
    }

    public function index_onDelete() {
        if (method_exists($this->controller, 'onDelete')) {
            return call_user_func_array([$this->controller, 'onDelete'], func_get_args());
        }

        $checkedIds = post('checked');

        $config = $this->cardsGetConfig();
        $model = new $config->modelClass;

        $query = $model->newQuery();
        $query->whereIn('id', $checkedIds);

        $records = $query->get();

        if (count($records) > 0) {
            foreach ($records as $record) {
                $record->delete();
            }

            Flash::success(Lang::get('backend::lang.list.delete_selected_success'));
        } else {
            Flash::error(Lang::get('backend::lang.list.delete_selected_empty'));
        }

        return $this->makeCards()->onRefresh();
    }

    protected function makeCards() {
        $config = $this->cardsGetConfig();

        $model = new $config->modelClass;

        $cardsConfig = $this->makeConfig($config->cards, ['fields']);
        $cardsConfig->model = $model;

        $configFieldsToTransfer = [
            'recordUrl',
            'recordsPerPage',
            'noRecordsMessage'
        ];

        foreach ($configFieldsToTransfer as $field) {
            if (isset($config->{$field})) {
                $cardsConfig->{$field} = $config->{$field};
            }
        }

        $widget = $this->makeWidget('JMSystems\BackendExtensions\Widgets\Cards', $cardsConfig);
        $widget->bindToController();

        return $widget;
    }

    protected function makeToolbar() {
        $config = $this->cardsGetConfig();

        // toolbar is optional
        if (!isset($config->toolbar)) return null;
        $toolbarConfig = $config->toolbar;

        $widget = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
        $widget->cssClasses[] = 'list-header';
        $widget->bindToController();

        if ($searchWidget = $widget->getSearchWidget()) {
            $searchWidget->bindEvent('search.submit', function () use ($widget, $searchWidget) {
                $this->cards->setSearchTerm($searchWidget->getActiveTerm());
                return $this->cards->onRefresh();
            });

            $this->cards->setSearchTerm($searchWidget->getActiveTerm());
        }

        return $widget;
    }

    protected function cardsGetConfig() {
        if ($this->config) return $this->config;
        return $this->config = $this->makeConfig($this->cardsConfig, ['modelClass', 'cards']);
    }

}