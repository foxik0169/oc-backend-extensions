import preact, {Component} from 'preact'
import Card from './card'

class Cards extends Component {

    constructor(props) {
        super(props)

        this.onResponse = this.onResponse.bind(this)
        this.state = {
            items: props.items.map(item => Object.assign({}, item, {selected: false})),
        }
    }

    getSelected() {
        return this.state.items.filter(item => item.selected).map(item => item.id)
    }

    componentWillMount() {
        $(document).on('ajaxSuccess', this.onResponse.bind(this))
    }

    componentWillUnmount() {
        $(document).off('ajaxSuccess', this.onResponse.bind(this))
    }

    componentWillUpdate(props, state) {
        const anySelected = state.items.filter(item => item.selected).length > 0
        this.props.enableDisableElements.forEach(element => {
            if (anySelected) element.disabled = false
            else element.disabled = true
        })
    }

    render(props, state) {
        return <div className="bex-cards">
            {state.items.map((item) => 
                <Card {...item}
                    key={item.id}
                    onToggleSelect={this.onToggleSelect.bind(this, item)}
                    onDelete={this.onDelete.bind(this, item)} />)}     
            {state.items.length <= 0 && 
                <div class="no-data">{props.noRecordsMessage}</div>}
        </div>
    }

    onResponse(event, context, data, statusOrData, jqXHR) {
        // the response fingerprint is different in certain cases
        // nasty hack to support them all
        const state = data[`bexCards_${this.props.id}`] || statusOrData[`bexCards_${this.props.id}`]
        
        if (!state) return
        this.setState({
            items: JSON.parse(state.items)
        })
    }

    onToggleSelect(targetItem) {
        const items = this.state.items.map(item => {
            if (item.url === targetItem.url)
                return Object.assign({}, item, {selected: !item.selected})

            return item
        })
        this.setState({
            items
        })
    }
    
    onDelete(targetItem) {
        const items = this.state.items.filter(item => item !== targetItem)
        this.setState({
            items
        })
    }

}

Cards.defaultProps = {
    id: null,
    items: [],
    searchElement: null,
    enableDisableElements: [],
    noRecordsMessage: null
}

export default Cards