import preact from 'preact'
import Cards from './cards'

// global access
$.bex = $.bex || {}
$.bex.cards = {}

const spawnElements = () => {
    const elements = document.querySelectorAll('[data-bex-cards]')
    const refHandler = (id, ref) => $.bex.cards[id] = ref

    elements.forEach(element => {
        const searchElement = document.querySelector(element.dataset.bexSearch)
        const enableDisableElements = document.querySelectorAll('[data-bex-cards-enable-selected]')
        
        preact.render(<Cards id={element.id}
                             ref={refHandler.bind(null, element.id)}
                             noRecordsMessage={element.dataset.bexNoRecordsMessage}
                             items={JSON.parse(element.dataset.bexItems)}
                             searchElement={searchElement}
                             enableDisableElements={enableDisableElements} />, element)
    })
}

if (document.readyState === 'loading')
    document.addEventListener('DOMContentLoaded', spawnElements)
else
    spawnElements()