import preact, {Component} from 'preact'
import classNames from 'classnames'
import trunc from 'trunc-html'

const defaultImage = '/plugins/jmsystems/backendextensions/assets/img/no-image.svg'

class Card extends Component {

    constructor(props) {
        super(props)

        this.onToggleSelect = this.onToggleSelect.bind(this)
        this.onDelete = this.onDelete.bind(this)
    }

    onToggleSelect(e) {
        e.preventDefault()
        e.stopPropagation()
        this.props.onToggleSelect()
    }

    onDelete(e) {
        e.preventDefault()
        e.stopPropagation()
        this.props.onDelete()
    }

    render(props, state) {
        const cardClassNames = classNames('bex-card', {
            'selected': props.selected
        })

        return <a className={cardClassNames} href={props.url}>
            <div className="bex-card-actions clearfix">
                <button type="button" className="bex-select pull-left" onClickCapture={this.onToggleSelect}>
                    <i className={props.selected ? "icon-minus" : "icon-plus"}></i>
                </button>
            </div>
            <img src={props.image || defaultImage} alt="Card image"/>
            <div className="bex-card-info">
                <h3 className="bex-card-title">{props.title}</h3>
                {props.content && <div className="bex-card-content" 
                     dangerouslySetInnerHTML={{__html: trunc(props.content, 120).html}}></div>}
            </div>
        </a>
    }

}

Card.defaultProps = {
    title: 'Card title',
    content: null,
    image: defaultImage,
    url: '#',
    selected: false,

    onToggleSelect: () => null,
    onDelete: () => null,
}

export default Card