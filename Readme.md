# Backend extensions for October CMS
Backend extensions is an open source plugin for October CMS
developed by JM Systems. We enjoy working with this platform so
we decided that we will contribute parts of our work to the public.

This plugin contains widgets, behaviors and other backend goodies
for faster development and better client experience.

## Provided extensions

### Cards - Controller behavior, Widget
![Cards](https://daniel.jmsystems.sk/static/oc-bex/oc-bex-cards.png)

Cards provide alternative to October CMS internal list controller
behavior with different interface. As filters are not currently 
supported, this behavior should be used only with small to medium 
dataset. Search and pagination **is** supported.

#### Usage
To use cards you will need to put `CardsController` behavior to 
`$implements` in your controller. Because `CardsController` 
behavior implements index(), you will have to remove the default 
`ListController` behavior from your `$implements` if you used it
before.

Cards use configuration file in a similar way to October CMS internal
behaviors. First you will need to specify `$cardsConfig` member variable.

```php
<?php namespace Acme\MyPlugin\Controllers;

use Backend\Classes\Controller;

class ControllerWithCards extends Controller
{
    ...
    public $implement = [
        ...,
        'JMSystems\BackendExtensions\Behaviors\CardsController',  // behavior class
        ...
    ];

    public $cardsConfig = 'config_cards.yaml';  // controller config
    ...
}
```

The controller config file should look something like this:

```yaml
title: ModelName
modelClass: Acme\MyPlugin\Models\DemoModel
recordUrl: 'acme/myplugin/controllerwithcards/update/:id'
recordsPerPage: 8
cards: $/acme/myplugin/models/demomodel/cards.yaml
noRecordsMessage: 'backend::lang.list.no_records'
toolbar:
  buttons: list_toolbar
  search:
    prompt: 'backend::lang.list.search_prompt'
```
As you can see, in the controller config we specify another two files:
 * `$/acme/myplugin/models/demomodel/cards.yaml` - the configuration 
 file for the cards widget
 * `list_toolbar` - partial that contains buttons in toolbar

In the cards config you can specify how the fields of a model should
map to fields of a card. For example:

```yaml
fields:
  title: name
  content: description
  image: thumbnail
```

If you need to support removal of the entries, you will need to edit
the toolbar template and add `onclick` and `data-bex-cards-enable-selected`
from the following snippet to your delete button.

```html
<div data-control="toolbar">
    ...
    <button
            class="btn btn-default oc-icon-trash-o"
            disabled="disabled"
            onclick="$(this).data('request-data', {
                checked: $.bex.cards.Cards.getSelected()
            })"
            data-request="onDelete"
            data-request-confirm="<?= e(trans('backend::lang.list.delete_selected_confirm')) ?>"
            data-bex-cards-enable-selected
            data-request-success="$(this).prop('disabled', true)"
            data-stripe-load-indicator>
        <?= e(trans('backend::lang.list.delete_selected')) ?>
    </button>
</div>

```

That's it! You've successfully added a cards view to your backend.

## License
Backend extensions package is licenced under MIT.