<?php return [
    'plugin' => [
        'name' => 'BackendExtensions',
        'description' => 'A plugin that provides extended backend functionality - widgets, backend controller behaviours and more.'
    ]
];