const _ = require('lodash')
const webpack = require('webpack')
const glob = require('glob')
const path = require('path')

// map folder structure to output bundles
const widgets = glob.sync('./src/widgets/**/*.entry.js?(x)')
const entries = _.keyBy(widgets, url => `${url.match(/\w+(?=\.entry\.js)/)[0]}.widget`)

module.exports = {
  mode: 'production',
  entry: entries,
  output: {
    filename: '[name].js',
    path: path.join(__dirname, '/assets/js')
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
        resolve: { 
          extensions: ['.wasm', '.mjs', '.js', '.jsx']
        },
      }
    ]
  },
  plugins: [
    new webpack.ProgressPlugin()
  ],
  devtool: 'eval',
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          name: "vendor",
          chunks: "initial",
          enforce: true
        }
      }
    }
  }
}